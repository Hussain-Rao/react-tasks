import React from 'react';
import logo from './logo.svg';
import './App.css';
import Hello from './Components/Hello';
import Greet from './Components/Greet'
import Welcome from './Components/Welcome';
import Message from './Components/Message';
import Counter from './Components/Counter';
import UserGreeting from './Components/UserGreeting';
function App() {
  return (
    <div className="App">
      <UserGreeting/>
      {/* <Counter/> */}
      {/* <Message/> */}
      {/* <Greet name="Hussain"/>
      <Greet name="Rao">
        <p>This is the children prop</p>
      </Greet>
      <Greet name="Hashim">
        <button>click me</button>
      </Greet> */}
      {/* <Welcome name="Hussain">
        <p>This is class component and its a children prop</p>
      </Welcome>  */}
      {/* <Hello/> */}
      
    </div>
  );
}

export default App;
