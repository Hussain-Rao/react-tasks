import React, { Component } from 'react'

class Counter extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             counter: 0
        }
    }
    increment = () =>{
        // this.setState({
        //     counter:this.state.counter+1
        // },()=>(console.log(this.state.counter)))
        this.setState(prevState =>({
            counter : prevState.counter+1
        }))
    }
    incrementFive = () =>{
        this.increment();
        this.increment();
        this.increment();
        this.increment();
        this.increment();
    }
    render() {
        return (
            <div>
                <p>count-{this.state.counter}</p>
                <button onClick={this.incrementFive}>increment</button>
            </div>
        )
    }
}

export default Counter
